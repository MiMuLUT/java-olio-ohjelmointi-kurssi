package textfield;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class FXMLDocumentController implements Initializable {
	@FXML
	private Label label;
	
	@FXML
	private TextField actionText;
	
	@FXML
	private void handleButtonAction(ActionEvent event) {
		System.out.println("Hello World!");
		label.setText(actionText.getText());
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
	}

}
