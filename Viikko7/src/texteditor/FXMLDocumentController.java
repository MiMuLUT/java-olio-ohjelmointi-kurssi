//Mikko Mustonen 06.06.2017
package texteditor;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class FXMLDocumentController implements Initializable {
	@FXML private TextField filePath;
	@FXML private Button btnOpen;
	@FXML private Button btnSave;
	@FXML private TextArea textArea;
	@FXML private Label errorLabel;
	
	@FXML private void btnOpen(ActionEvent event) {
		String temp = ReadAndWriteIO.readFile(filePath.getText());
		errorLabel.setText("");
		if (temp != null) {
			textArea.setText(temp);
		} else {
			errorLabel.setText("Virhe tiedoston lukemisessa!");
		}
	}
	
	@FXML private void btnSave(ActionEvent event) {
		errorLabel.setText("");
		if (ReadAndWriteIO.writeFile(filePath.getText(), textArea.getText())) {
			errorLabel.setText("Virhe tiedoston kirjoittamisessa!");
		}
	}
	
	@Override public void initialize(URL url, ResourceBundle rb) {
		
	}
}
