//Mikko Mustonen 06.06.2017
package texteditor;

import java.io.*;

public class ReadAndWriteIO {
	
	private ReadAndWriteIO() {
		
	}
	
	public static String readFile(String filepath) {
		String text = null;
		try {
			BufferedReader in = new BufferedReader(new FileReader(filepath));
			String inputLine;
			text = "";
			
			while ((inputLine = in.readLine()) != null) {
				text += inputLine;
			}
			in.close();
		} catch (IOException e) {
			System.out.println("Virhe tiedoston lukemisessa!");
		}
		return text;
	}
	
	public static boolean writeFile(String filepath, String text) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(filepath));
			out.write(text);
			out.close();
			return false;
		} catch (IOException e) {
			System.out.println("Virhe tiedoston kirjoittamisessa!");
			return true;
		}
	}
}
