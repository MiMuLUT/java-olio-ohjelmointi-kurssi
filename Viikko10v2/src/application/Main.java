//Mikko Mustonen 12.06.2017
package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

public class Main extends Application {
	TabPane tabPane = new TabPane();

	public static void main(String[] args) {
		Application.launch(args);
	}

	private void newTab() {
		final Tab tab = new Tab("temp");
		VBox vbox = new VBox();
		HBox hbox = new HBox();
		Button btnBack = new Button("<");
		Button btnForward = new Button(">");
		Button btnRefresh = new Button("Refresh");
		final TextField fieldUrlBar = new TextField();
		Button btnGo = new Button("Go");
		Button btnJs = new Button("JS");
		Button btnInit = new Button("Init");
		final WebView webView = new WebView();
		webView.getEngine().load("https://duckduckgo.com/");
		fieldUrlBar.setText(webView.getEngine().getLocation());
		tab.setText(webView.getEngine().getLocation());
		EventHandler<ActionEvent> go = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (!fieldUrlBar.getText().isEmpty()) {
					if (!fieldUrlBar.getText().contains(".") || fieldUrlBar.getText().contains(" ")) {
						fieldUrlBar.setText("https://duckduckgo.com/?q=" + fieldUrlBar.getText());
					} else if (fieldUrlBar.getText().equals("index.html") || fieldUrlBar.getText().equals(webView.getEngine().getLocation())) {
						fieldUrlBar.setText(getClass().getResource("index.html").toExternalForm());
					} else if (!fieldUrlBar.getText().contains("http://") && !fieldUrlBar.getText().contains("https://")) {
						fieldUrlBar.setText("http://" + fieldUrlBar.getText());
					}
					webView.getEngine().load(fieldUrlBar.getText());
				}
				fieldUrlBar.setText(webView.getEngine().getLocation());
				tab.setText(webView.getEngine().getLocation());
			}
		};
		btnBack.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (webView.getEngine().getHistory().getCurrentIndex() > 0) {
					webView.getEngine().getHistory().go(-1);
					fieldUrlBar.setText(webView.getEngine().getLocation());
				}
			}
		});
		btnForward.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				if (webView.getEngine().getHistory().getCurrentIndex() < webView.getEngine().getHistory().getEntries().size() - 1) {
					webView.getEngine().getHistory().go(1);
					fieldUrlBar.setText(webView.getEngine().getLocation());
				}
			}
		});
		btnRefresh.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				webView.getEngine().reload();
				fieldUrlBar.setText(webView.getEngine().getLocation());
			}
		});
		fieldUrlBar.setOnAction(go);
		btnGo.setOnAction(go);
		btnJs.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				webView.getEngine().executeScript("document.shoutOut()");
			}
		});
		btnInit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				webView.getEngine().executeScript("initialize()");
			}
		});
		hbox.getChildren().addAll(btnBack, btnForward, btnRefresh, fieldUrlBar, btnGo, btnJs, btnInit);
		hbox.setPadding(new Insets(5, 5, 5, 5));
		hbox.setSpacing(5);
		hbox.setHgrow(fieldUrlBar, Priority.ALWAYS);
		vbox.setVgrow(webView, Priority.ALWAYS);
		vbox.getChildren().addAll(hbox, webView);
		tab.setContent(vbox);
		tabPane.getTabs().add(tabPane.getTabs().size() - 1, tab);
		tabPane.getSelectionModel().select(tab);
	}

	@Override
	public void start(Stage primaryStage) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.ALL_TABS);
		tabPane.setTabMaxWidth(200);
		Button btnNew = new Button();
		btnNew.setText("+");
		btnNew.setStyle("-fx-background-color: none;");
		btnNew.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				newTab();
			}
		});
		Scene scene = new Scene(tabPane, 800, 600);
		Tab tabNew = new Tab();
		tabNew.setClosable(false);
		tabNew.setGraphic(btnNew);
		tabPane.getTabs().add(tabNew);
		primaryStage.setTitle("Goofy browser");
		primaryStage.setScene(scene);
		primaryStage.show();
		newTab();
	}

}