//Mikko Mustonen 13.06.2017
package application;

import java.util.ArrayList;

import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class ShapeHandler {
	private static ShapeHandler sHandler;
	private Circle circle1 = null;
	private Circle circle2 = null;
	private AnchorPane pane;
	private ArrayList<Circle> points = new ArrayList<Circle>();
	private ArrayList<Line> lines = new ArrayList<Line>();
	private boolean lineRender = true;

	private ShapeHandler() {
		
	}

	public static ShapeHandler getSH() {
		if (sHandler == null) {
			sHandler = new ShapeHandler();
		}
		return sHandler;
	}
	
	public void toggleLineRender() {
		if (lineRender) {
			lineRender = false;
		} else {
			lineRender = true;
		}
	}
	
	public void addCircle(Circle circle) {
		points.add(circle);
	}
	
	public void setPane(AnchorPane root) {
		pane = root;
	}
	
	public void setCircle(Circle circle) {
		if (circle1 == null) {
			circle1 = circle;
			circle1.setFill(Color.RED);
		} else if (circle1 != circle) {
			circle2 = circle;
			drawLine();
		} else {
			circle1.setFill(Color.MEDIUMBLUE);
			circle1 = null;
		}
	}

	private void drawLine() {
		Line line = new Line(circle1.getCenterX(), circle1.getCenterY(), circle2.getCenterX(), circle2.getCenterY());
		line.setMouseTransparent(true);
		line.setStrokeWidth(5);
		line.setStroke(Color.MEDIUMBLUE);
		circle1.setFill(Color.MEDIUMBLUE);
		circle2.setFill(Color.MEDIUMBLUE);
		circle1 = null;
		circle2 = null;
		if (lineRender) {
			pane.getChildren().removeAll(lines);
			lines.clear();
		}
		pane.getChildren().add(line);
		lines.add(line);
	}
}
