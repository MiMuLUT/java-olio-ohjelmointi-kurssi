//Mikko Mustonen 13.06.2017
package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		try {
			final AnchorPane root = new AnchorPane();
			Button btnToggle = new Button("one/many");
			btnToggle.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent e) {
					ShapeHandler.getSH().toggleLineRender();
				}
			});
			root.getChildren().add(btnToggle);
			ShapeHandler.getSH().setPane(root);
			root.setPrefWidth(677);
			root.setPrefHeight(1024);
			root.setStyle("-fx-background-image:url('" + getClass().getResource("Suomen-kartta.jpg").toExternalForm() + "')");
			root.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override public void handle(MouseEvent event) {
					if (event.getTarget().equals(root)) {
						root.getChildren().add(new Point().makePoint(event));
					}
				}
			});
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
