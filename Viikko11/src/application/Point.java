//Mikko Mustonen 13.06.2017
package application;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Point {
	private String name = "piste";
	private Circle circle;

	public Point() {
		
	}

	public Point(String new_name) {
		name = new_name;
	}

	public Node makePoint(final MouseEvent event) {
		circle = new Circle(event.getSceneX(), event.getSceneY(), 10, Color.MEDIUMBLUE);
		ShapeHandler.getSH().addCircle(circle);
		circle.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent clickEvent) {
				ShapeHandler.getSH().setCircle(circle);
				System.out.println("Hei olen " + name + "!");
			}
		});
		return circle;
	}
}
