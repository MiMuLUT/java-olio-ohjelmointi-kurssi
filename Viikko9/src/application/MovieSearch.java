//Mikko Mustonen 08.06.2017
package application;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class MovieSearch {

	DataGather dg = new DataGather();

	public ObservableList<String> getList(String search, ArrayList<String> ids, LocalDate datee) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		LocalDate date = datee;
		String correctDate;
		if (date != null) {
			correctDate = formatter.format(date);
		} else {
			correctDate = null;
		}

		ArrayList<String> names = new ArrayList<String>();
		ArrayList<String> times = new ArrayList<String>();
		ArrayList<String> theaters = new ArrayList<String>();
		ArrayList<String> finalList = new ArrayList<String>();
		for (int k = 0; k < ids.size(); k++) {
			String urli = "http://www.finnkino.fi/xml/Schedule/?area=" + ids.get(k) + "&dt=" + correctDate;
			Document doc = dg.gatherData(urli);
			NodeList nameNodes = doc.getElementsByTagName("OriginalTitle");
			NodeList timeNodes = doc.getElementsByTagName("dttmShowStart");
			NodeList theaterNodes = doc.getElementsByTagName("Theatre");
			for (int i = 0; i < nameNodes.getLength(); i++) {
				names.add(nameNodes.item(i).getTextContent());
				times.add(timeNodes.item(i).getTextContent());
				theaters.add(theaterNodes.item(i).getTextContent());
			}
		}
		for (int i = 0; i < names.size(); i++) {
			if (names.get(i).toLowerCase().contains(search.toLowerCase()) && !finalList.contains(names.get(i))) {
				finalList.add(names.get(i));
				for (int j = i; j < names.size(); j++) {
					if (names.get(i).equals(names.get(j))) {
						finalList.add('\t' + times.get(j).substring(11, 16) + '\t' + theaters.get(j));
					}
				}
			}
		}
		ObservableList<String> observableList = FXCollections.observableArrayList(finalList);
		return observableList;
	}
}
