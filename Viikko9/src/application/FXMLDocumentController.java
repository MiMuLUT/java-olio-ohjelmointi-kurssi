//Mikko Mustonen 08.06.2017
package application;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
	private Theaters data = new Theaters();
	private Schedule schedule = new Schedule();
	private MovieSearch search = new MovieSearch();
	private Map<String, String> theaters;
	private ArrayList<String> ids = new ArrayList<String>();;
	
	@FXML
    private ComboBox<String> choiceTheater;

    @FXML
    private DatePicker selectDay;

    @FXML
    private TextField fieldStart;

    @FXML
    private TextField fieldEnd;

    @FXML
    private Button btnMovies;

    @FXML
    private TextField fieldName;

    @FXML
    private Button btnName;
    
    @FXML
    private ListView<String> listView;

    @FXML
    void handleListMovies(ActionEvent event) {
    	listView.setItems(schedule.getList(getID(), selectDay.getValue(), fieldStart.getText(), fieldEnd.getText()));
    }

    @FXML
    void handleSearchName(ActionEvent event) {
    	listView.setItems(search.getList(fieldName.getText(), ids, selectDay.getValue()));
    }
    
    private String getID() {
    	String id = null;
    	for (Entry<String, String> e : theaters.entrySet()) {
			if (choiceTheater.getValue().equals(e.getValue())) {
				id = e.getKey();
				break;
			}
		}
		return id;
    }

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		selectDay.setValue(LocalDate.now());
		theaters = data.getMap();
		for (Entry<String, String> e : theaters.entrySet()) {
			if (!e.getValue().contains(":") && !e.getValue().contains("/") && !e.getValue().contains("Helsinki") && !e.getValue().contains("Espoo") && !e.getValue().contains("Vantaa")) {
				ids.add(e.getKey());
			}
			choiceTheater.getItems().add(e.getValue());
		}
    	choiceTheater.getSelectionModel().select(0);
	}
}
