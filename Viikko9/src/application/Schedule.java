//Mikko Mustonen 08.06.2017
package application;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Schedule {
	
	private String urli;
	private ArrayList<String> list = new ArrayList<String>();
	private String startTime;
	private String endTime;
	
	private void parseData() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date dateStart = null, dateEnd = null, dateTime = null;
		try {
			dateStart = format.parse(startTime);
			dateEnd = format.parse(endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		DataGather dg = new DataGather();
		Document doc = dg.gatherData(urli);
		list.clear();
		NodeList names = doc.getElementsByTagName("OriginalTitle");
		NodeList times = doc.getElementsByTagName("dttmShowStart");
		for (int i = 0; i < names.getLength(); i++) {
			try {
				dateTime = format.parse(times.item(i).getTextContent());
				if (dateTime.before(dateEnd) && dateTime.after(dateStart) || dateTime.compareTo(dateEnd) == 0 || dateTime.compareTo(dateStart) == 0) {
					list.add(times.item(i).getTextContent().substring(11, 16) + "\t" + names.item(i).getTextContent());
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}
	
	public ObservableList<String> getList(String id, LocalDate datee, String start, String end) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    	LocalDate date = datee;
    	String correctDate;
    	if (date != null) {
    		correctDate = formatter.format(date);
    	} else {
    		correctDate = null;
    	}
    	if (!start.isEmpty()) {
    		startTime = datee + "T" + start + ":00";
    	} else {
    		startTime = datee + "T" + "00:00" + ":00";
    	}
		if (!end.isEmpty()) {
			endTime = datee + "T" + end + ":00";
		} else {
			endTime = datee + "T" + "24:00" + ":00";
		}
		urli = "http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + correctDate;
		parseData();
		ObservableList<String> observableList = FXCollections.observableArrayList(list);
		return observableList;
	}
}
