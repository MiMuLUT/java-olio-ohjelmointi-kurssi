//Mikko Mustonen 08.06.2017
package application;

import java.util.LinkedHashMap;
import java.util.Map;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class Theaters {
	private Map<String, String> m = null;
	
	private void parseData() {
		DataGather dg = new DataGather();
		Document doc = dg.gatherData("http://www.finnkino.fi/xml/TheatreAreas/");
		NodeList names = doc.getElementsByTagName("Name");
		NodeList IDs = doc.getElementsByTagName("ID");
		for (int i = 0; i < names.getLength(); i++) {
			m.put(IDs.item(i).getTextContent(), names.item(i).getTextContent());
		}
	}
	
	public Map<String, String> getMap() {
		if (m == null) {
			m = new LinkedHashMap<String, String>();
			parseData();
		}
		return m;
	}
}
