//Mikko Mustonen 08.06.2017
package application;

import java.io.*;
import java.net.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class DataGather {
	
	private Document doc;
	
	public Document gatherData(String urli) {
		try {
			URL url = new URL(urli);
			BufferedReader in = new BufferedReader (new InputStreamReader(url.openStream()));
			String inputLine, total="";
			
			while ((inputLine = in.readLine()) != null) {
				total += inputLine + "\n";
			}
			in.close();
			return makeDoc(total);
		} catch (MalformedURLException e) {
			System.err.println("Caught MalformedURLException!");
		} catch (IOException e) {
			System.err.println("Caught IOException!");
		}
		return null;	
	}
	
	private Document makeDoc(String total) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(new InputSource(new StringReader(total)));
			doc.getDocumentElement().normalize();
			return doc;
		} catch (ParserConfigurationException e) {
			System.err.println("Caught ParserConfigurationException!");
		} catch (IOException e) {
			System.err.println("Caught IOException!");
		} catch (SAXException e) {
			System.err.println("Caught SAXException!");
		}
		return null;
	}
}
