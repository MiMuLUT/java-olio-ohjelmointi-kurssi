//Mikko Mustonen 30.05.2017
public class Dog {
	
	private String name;
	private String[] say;

	public Dog(String new_name) {
		if (!new_name.trim().isEmpty()) {
			name = new_name;
		} else {
			name = "Doge";
		}
		System.out.println("Hei, nimeni on " + name);
		say = new String[] {"Much wow!"};
	}
	
	public boolean speak(String new_say) {
		if (new_say.trim().isEmpty()) {
			System.out.println(name + ": " + say);
			return true;
		}
		say = new_say.split(" ");
		for (String part: say) {
			try {
				Integer.parseInt(part);
				System.out.println("Such integer" + ": " + part);	
			} catch (NumberFormatException e) {
				if (part.equalsIgnoreCase("true") || part.equalsIgnoreCase("false")) {
					System.out.println("Such boolean" + ": " + part);
				} else {
					System.out.println(part);
				}
			}
		}
		return false;
	}
}
