//Mikko Mustonen 30.05.2017
import java.util.Scanner;

public class Mainclass {

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.print("Anna koiralle nimi: ");
		String n = reader.nextLine();
		Dog dog = new Dog(n);
		
		do {
			System.out.print("Mitä koira sanoo: ");
			n = reader.nextLine();
		} while (dog.speak(n));
		reader.close();
	}
}
