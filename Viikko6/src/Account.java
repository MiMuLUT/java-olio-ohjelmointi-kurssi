//Mikko Mustonen 05.06.2017

public abstract class Account {
	protected String number;
	protected int balance;
	
	public Account(String new_number, int new_balance) {
		number = new_number;
		balance = new_balance;
	}

	public String getNumber() {
		return number;
	}
	
	public void setNumber(String new_number) {
		number = new_number;
	}

	public int getBalance() {
		return balance;
	}
	
	public void addBalance(int ammount) {
		balance += ammount;
	}
	
	public void deductBalance(int ammount) {
		if (ammount < balance) {
			balance -= ammount;
		} else {
			System.out.println("Kate ei riitä.");
		}
	}
	

}

class Debit extends Account {
	public Debit(String new_number, int new_balance) {
		super(new_number, new_balance);
	}
}

class Credit extends Account {
	private int limit;
	
	public Credit(String new_number, int new_balance, int new_limit) {
		super(new_number, new_balance);
		limit = new_limit;
	}
	
	public int getLimit() {
		return limit;
	}
	
	public void setLimit(int new_limit) {
		limit = new_limit;
	}
	
	public void deductBalance(int ammount) {
		if (ammount < balance + limit) {
			balance -= ammount;
		} else {
			System.out.println("Kate ei riitä.");
		}
	}
}