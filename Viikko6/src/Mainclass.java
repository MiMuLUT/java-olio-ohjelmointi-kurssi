//Mikko Mustonen 05.06.2017
import java.util.InputMismatchException;
import java.util.Scanner;

public class Mainclass {

	public static void main(String[] args) {
		Bank bank = new Bank();
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
			System.out.println("1) Lisää tavallinen tili\n2) Lisää luotollinen tili");
			System.out.println("3) Tallenna tilille rahaa\n4) Nosta tililtä");
			System.out.println("5) Poista tili\n6) Tulosta tili");
			System.out.println("7) Tulosta kaikki tilit\n0) Lopeta");
			System.out.print("Valintasi: ");
			try {
				int choice = sc.nextInt();
				sc.nextLine();
				switch (choice) {
				case 0:
					break;
				case 1:
					System.out.print("Syötä tilinumero: ");
					String number = sc.nextLine();
					if (bank.findAccount(number) == null) {
						System.out.print("Syötä rahamäärä: ");
						int money = sc.nextInt();
						sc.nextLine();
						bank.addAccount(number, "debit", money, 0);
					} else {
						System.out.println("Tili " + number + " on jo olemassa.");
					}
					continue;
				case 2:
					System.out.print("Syötä tilinumero: ");
					number = sc.nextLine();
					if (bank.findAccount(number) == null) {
						System.out.print("Syötä rahamäärä: ");
						int money = sc.nextInt();
						sc.nextLine();
						System.out.print("Syötä luottoraja: ");
						int limit = sc.nextInt();
						sc.nextLine();
						bank.addAccount(number, "credit", money, limit);
					} else {
						System.out.println("Tili " + number + " on jo olemassa.");
					}
					continue;
				case 3:
					System.out.print("Syötä tilinumero: ");
					number = sc.nextLine();
					System.out.print("Syötä rahamäärä: ");
					int money = sc.nextInt();
					sc.nextLine();
					bank.deposit(number, money);
					continue;
				case 4:
					System.out.print("Syötä tilinumero: ");
					number = sc.nextLine();
					System.out.print("Syötä rahamäärä: ");
					money = sc.nextInt();
					sc.nextLine();
					bank.withdraw(number, money);
					continue;
				case 5:
					System.out.print("Syötä poistettava tilinumero: ");
					number = sc.nextLine();
					bank.delAccount(number);
					continue;
				case 6:
					System.out.print("Syötä tulostettava tilinumero: ");
					number = sc.nextLine();
					bank.getAccount(number);
					continue;
				case 7:
					bank.getAll();
					continue;
				default:
					System.out.println("Valinta ei kelpaa.");
					continue;
				}
				break;
			} catch (InputMismatchException e) {
				System.out.println("Valinta ei kelpaa.");
				sc.nextLine();
			}
		}
		sc.close();
	}
}
