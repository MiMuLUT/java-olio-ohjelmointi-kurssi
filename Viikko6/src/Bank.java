//Mikko Mustonen 05.06.2017
import java.util.ArrayList;

public class Bank {
	private ArrayList<Account> accounts = new ArrayList<Account>();

	public Account findAccount(String number) {
		if (!accounts.isEmpty()) {
			for (Account a: accounts) {
				if (a.getNumber().equals(number)) {
					return a;
				}
			}
		}
		return null;
	}

	public void addAccount(String number, String type, int money, int limit) {
		if (type.equals("debit")) {
			accounts.add(new Debit(number, money));
		} else {
			accounts.add(new Credit(number, money, limit));
		}
		System.out.println("Tili luotu.");
	}

	public void deposit(String number, int money) {
		Account acc = findAccount(number);
		if (acc != null) {
			acc.addBalance(money);
		} else {
			System.out.println("Tiliä " + number + " ei ole.");
		}
	}

	public void withdraw(String number, int money) {
		Account acc = findAccount(number);
		if (acc != null) {
			acc.deductBalance(money);
		} else {
			System.out.println("Tiliä " + number + " ei ole.");
		}
	}

	public void delAccount(String number) {
		Account acc = findAccount(number);
		if (acc != null) {
			accounts.remove(acc);
			System.out.println("Tili poistettu.");
		} else {
			System.out.println("Tiliä " + number + " ei ole.");
		}
	}

	public void getAccount(String number) {
		Account acc = findAccount(number);
		if (acc != null) {
			if (acc instanceof Debit) {
				System.out.println("Tilinumero: " + number + " Tilillä rahaa: " + acc.getBalance());
			} else if (acc instanceof Credit){
				System.out.println("Tilinumero: " + number + " Tilillä rahaa: " + acc.getBalance() + " Luottoraja: " + ((Credit) acc).getLimit());
			}
		} else {
			System.out.println("Tiliä " + number + " ei ole.");
		}
	}

	public void getAll() {
//		if (!accounts.isEmpty()) {
			System.out.println("Kaikki tilit:");
			for (Account a: accounts) {
				if (a instanceof Debit) {
					System.out.println("Tilinumero: " + a.getNumber() + " Tilillä rahaa: " + a.getBalance());
				} else if (a instanceof Credit){
					System.out.println("Tilinumero: " + a.getNumber() + " Tilillä rahaa: " + a.getBalance() + " Luottoraja: " + ((Credit) a).getLimit());
				}
			}
//		} else {
//			System.out.println("Pankissa ei ole yhtään tiliä.");
//		}
	}
}
