//Mikko Mustonen 31.05.2017
import java.io.*;
import java.util.zip.*;

public class ReadAndWriteIO {
	
	public ReadAndWriteIO() {
		
	}
	
	public void readFile(String filepath) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(filepath));
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);
			}
			in.close();
		} catch (IOException e) {
			System.out.println("Virhe tiedoston lukemisessa!");
		}
	}
	
	public void readAndWrite(String inFile) {
		try {
			ZipInputStream zipStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(inFile)));
			zipStream.getNextEntry();
			BufferedReader in = new BufferedReader(new InputStreamReader(zipStream));
			String inputLine;
			
			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);
			}
			in.close();
			zipStream.close();
		} catch (IOException e) {
			System.out.println("Virhe tiedoston lukemisessa!");
		}
	}
}
