package v8_1;
//Mikko Mustonen 31.05.2017
import java.util.ArrayList;
import java.util.Locale;

public class BottleDispenser {
    
    private double money;
	private ArrayList<Bottle> bottles = new ArrayList<Bottle>();
	private static BottleDispenser disp = null;
    
    private BottleDispenser() {
    	Bottle bottle = new Bottle();
    	bottles.add(bottle);
    	bottle = new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2);
    	bottles.add(bottle);
    	bottle = new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0);
    	bottles.add(bottle);
    	bottle = new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5);
    	bottles.add(bottle);
    	bottle = new Bottle("Fanta Zero", "Coca-Cola", 0.5, 1.95);
    	bottles.add(bottle);
    	bottle = new Bottle("Fanta Zero", "Coca-Cola", 0.5, 1.95);
    	bottles.add(bottle);
    	
        money = 0;
    }
    
    public static BottleDispenser getInstance() {
    	if (disp == null) {
    		disp = new BottleDispenser();
    	}
    	return disp;
    }
    
	public boolean checkChoice(String n, int max) {
		try {
			int choice = Integer.parseInt(n);
			if (choice <= max && choice >= 0) {
				return true;
			}
		} catch (NumberFormatException e) {}
		return false;
	}
	
	public int getBottleCount() {
		return bottles.size();
	}
    
    public void listBottles() {
    	for (int i = 0; i < bottles.size(); i++) {
    		System.out.println(i+1 + ". " + "Nimi: " + bottles.get(i).getName());
    		System.out.println("\tKoko: " + bottles.get(i).getSize() + "\tHinta: " + bottles.get(i).getPrice());
    	}
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public void buyBottle(int indx) {
    	if (bottles.size() <= 0) {
    		System.out.println("Ei pulloja jäljellä!");
    	} else if (money - bottles.get(indx).getPrice() < 0){
    		System.out.println("Syötä rahaa ensin!");
    	} else {
    		money -= bottles.get(indx).getPrice();
    		System.out.println("KACHUNK! " + bottles.get(indx).getName() + " tipahti masiinasta!");
    		deleteBottle(indx);
    	}
    }
    
    public void returnMoney() {
        System.out.printf(Locale.FRANCE, "Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€%n", money);
        money = 0;
    }
    
    public void deleteBottle(int indx) {
    	if (bottles.size() <= 0) {
    		System.out.println("Ei pulloja jäljellä!");
    	} else {
        	bottles.remove(indx);
    	}
    }
}
