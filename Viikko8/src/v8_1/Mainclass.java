package v8_1;
//Mikko Mustonen 31.05.2017
import java.util.Scanner;

public class Mainclass {
	

	public static void main(String[] args) {
		BottleDispenser disp = BottleDispenser.getInstance();
		Scanner read = new Scanner(System.in);
		while (true) {
			System.out.println("\n*** LIMSA-AUTOMAATTI ***");
			System.out.println("1) Lisää rahaa koneeseen");
			System.out.println("2) Osta pullo");
			System.out.println("3) Ota rahat ulos");
			System.out.println("4) Listaa koneessa olevat pullot");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			String input = read.nextLine();
			
			if (disp.checkChoice(input, 5)) {
				int choice = Integer.parseInt(input);
				if (choice == 0) {
					break;
				} else if (choice == 1) {
					disp.addMoney();
				} else if (choice == 2) {
					disp.listBottles();
					System.out.print("Valintasi: ");
					input = read.nextLine();
					if (disp.checkChoice(input, disp.getBottleCount())) {
						choice = Integer.parseInt(input);
						disp.buyBottle(choice - 1);
					} else {
						System.out.println("Väärä valinta!");
					}
				} else if (choice == 3) {
					disp.returnMoney();
				} else if (choice == 4) {
					disp.listBottles();
				}
			} else {
				System.out.println("Väärä valinta!");
			}
		}
		read.close();
	}
}
