package v8_1;
//Mikko Mustonen 31.05.2017
public class Bottle {
	private String pname;
	private String manufacturer;
	private double size;
	private double price;
	
	public Bottle() {
		pname = "Pepsi Max";
		manufacturer = "Pepsi";
		size = 0.5;
		price = 1.80;
	}
	
	public Bottle(String name, String manuf, double siz, double pric) {
		pname = name;
		manufacturer = manuf;
		size = siz;
		price = pric;
	}
	
	public String getName() {
		return pname;
	}
	
	public String getManufacturer() {
		return manufacturer;
	}

	public double getSize() {
		return size;
	}

	public double getPrice() {
		return price;
	}
}
