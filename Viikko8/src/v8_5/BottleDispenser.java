package v8_5;
import java.text.DecimalFormat;
//Mikko Mustonen 31.05.2017
import java.util.ArrayList;
import java.util.Locale;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class BottleDispenser {
    
    private double money;
	private ArrayList<Bottle> bottles = new ArrayList<Bottle>();
	private static BottleDispenser disp = null;
    
    private BottleDispenser() {
    	Bottle bottle = new Bottle();
    	bottles.add(bottle);
    	bottle = new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2, 5);
    	bottles.add(bottle);
    	bottle = new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0, 5);
    	bottles.add(bottle);
    	bottle = new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5, 5);
    	bottles.add(bottle);
    	bottle = new Bottle("Fanta Zero", "Coca-Cola", 0.5, 1.95, 5);
    	bottles.add(bottle);
    	bottle = new Bottle("Fanta Zero", "Coca-Cola", 1.5, 2.95, 5);
    	bottles.add(bottle);
    	
        money = 0;
    }
    
    public static BottleDispenser getInstance() {
    	if (disp == null) {
    		disp = new BottleDispenser();
    	}
    	return disp;
    }
    
	public boolean checkChoice(String n, int max) {
		try {
			int choice = Integer.parseInt(n);
			if (choice <= max && choice >= 0) {
				return true;
			}
		} catch (NumberFormatException e) {}
		return false;
	}
	
	public ObservableList<String> getList(String mode) {
		ArrayList<String> list = new ArrayList<String>();
		for (Bottle a: bottles) {
			if (mode.equals("name")){
				String name = a.getName();
				if (!list.contains(name)) {
					list.add(name);
				}
			} else {
				String size = String.valueOf(a.getSize());
				if (!list.contains(size)) {
					list.add(size);
				}
			}
		}
		ObservableList<String> oList = FXCollections.observableArrayList(list);
		return oList;
	}
	
	public double getPrice(String name, String size) {
		if (name != null && size != null) {
			for (Bottle a: bottles) {
				if (a.getName().equals(name) && a.getSize() == Double.parseDouble(size)){
					return a.getPrice();
				}
			}
		}
		return 0;
	}
	
	public double getMoney() {
		DecimalFormat df = new DecimalFormat("#.00");
		return Double.valueOf(df.format(money));
	}
    
    public void addMoney(int number) {
        money += number;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    public String buyBottle(String name, String size) {
    	if (name != null && size != null) {
    		for (Bottle a: bottles) {
    			if (a.getName().equals(name) && a.getSize() == Double.parseDouble(size)) {
    				if (a.getCount() > 0) {
    					if (money - a.getPrice() >= 0) {
    						money -= a.getPrice();
    						a.setCount();
    						new Receipt().writeReceipt("Kuitti\n" + name + " " + size + "l " + a.getPrice() + "€");
    						return "KACHUNK! " + name + " tipahti masiinasta!";
    					} else {
    						return "Syötä rahaa ensin!";
    					}
    				} else {
    					return "Ei pulloja jäljellä!";
    				}
    			}
    		}
    	}
    	return "Valitse pullo ensin!";
    }
    
    public void returnMoney() {
        System.out.printf(Locale.FRANCE, "Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€%n", money);
        money = 0;
    }
}
