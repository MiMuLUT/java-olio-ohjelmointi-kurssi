//Mikko Mustonen 14.06.2017
package v8_5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Receipt {
	public void writeReceipt(String output) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter("receipt.txt"));
			out.write(output);
			out.close();
		} catch (IOException e) {
			System.out.println("Virhe tiedoston kirjoittamisessa!");
		}
	}
}
