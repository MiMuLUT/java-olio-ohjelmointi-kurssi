//Mikko Mustonen 06.06.2017
package v8_5;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

public class FXMLDocumentController implements Initializable {

    @FXML private Slider sliderMoney;
    @FXML private Label labelMoney;
    @FXML private Button addMoney;
    @FXML private Button returnMoney;
    @FXML private Button buyBottle;
    @FXML private ChoiceBox<String> bottleList;
    @FXML private ChoiceBox<String> sizeList;
    @FXML private Label labelConsole;
    @FXML private Label totalMoney;
    @FXML private Label labelPrice;
    BottleDispenser disp = BottleDispenser.getInstance();
    
    @FXML void btnAdd(ActionEvent event) {
    	disp.addMoney(Integer.parseInt(labelMoney.getText()));
    	sliderMoney.setValue(0);
    	totalMoney.setText(Double.toString(disp.getMoney()));
    }

    @FXML void btnBuy(ActionEvent event) {
    	labelConsole.setText(disp.buyBottle(bottleList.getValue(), sizeList.getValue()));
    	totalMoney.setText(Double.toString(disp.getMoney()));
    }

    @FXML void btnReturn(ActionEvent event) {
    	labelConsole.setText("Rahaa palautettiin: " + totalMoney.getText());
    	disp.returnMoney();
    	totalMoney.setText(Double.toString(disp.getMoney()));
    }
	
	@Override public void initialize(URL url, ResourceBundle rb) {
		bottleList.setItems(disp.getList("name"));
		sizeList.setItems(disp.getList("size"));
		bottleList.valueProperty().addListener(new ChangeListener<String>() {
			@Override public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				labelPrice.setText(Double.toString(disp.getPrice(bottleList.getValue(), sizeList.getValue())));
			}
		});
		sizeList.valueProperty().addListener(new ChangeListener<String>() {
			@Override public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				labelPrice.setText(Double.toString(disp.getPrice(bottleList.getValue(), sizeList.getValue())));
			}
		});
		labelMoney.textProperty().bind(sliderMoney.valueProperty().asString("%.0f"));
	}
}
