//Mikko Mustonen 12.06.2017
package application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class TabController implements Initializable{
	@FXML
	private TabPane tabPane;
	
	private int titleNum = 1;

    @FXML
    void handleActionNew(ActionEvent event) {
    	try {
			Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
			Tab tab = new Tab();
			tab.setText("Tab" + titleNum++);
			tab.setContent(root);
	    	tabPane.getTabs().add(tabPane.getTabs().size() - 1, tab);
	    	tabPane.getSelectionModel().select(tab);
		} catch(Exception e) {
			e.printStackTrace();
		}
    }
    
    @Override
	public void initialize(URL url, ResourceBundle rb) {
    	try {
			Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
			Tab tab = new Tab();
			tab.setText("Tab" + titleNum++);
			tab.setContent(root);
	    	tabPane.getTabs().add(tabPane.getTabs().size() - 1, tab);
	    	tabPane.getSelectionModel().select(tab);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
    
    public void setTabTitle(String title) {
    	tabPane.getSelectionModel().getSelectedItem().setText(title);
    }
}
