//Mikko Mustonen 12.06.2017
package application;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

public class FXMLDocumentController implements Initializable {
	@FXML
	private Button btnPrevious;

	@FXML
	private Button btnNext;

	@FXML
	private Button btnRefresh;

	@FXML
	private TextField textFieldURL;

	@FXML
	private Button btnGo;

	@FXML
	private Button btnJS;

	@FXML
	private Button btnInit;

	@FXML
	private WebView webView;
	
	@FXML
	private VBox mainBox;

	@FXML
	void handleActionGo(ActionEvent event) {
		if (!textFieldURL.getText().isEmpty()) {
			if (!textFieldURL.getText().contains(".") || textFieldURL.getText().contains(" ")) {
				textFieldURL.setText("https://duckduckgo.com/?q=" + textFieldURL.getText());
			} else if (textFieldURL.getText().equals("index.html") || textFieldURL.getText().equals(webView.getEngine().getLocation())) {
				textFieldURL.setText(getClass().getResource("index.html").toExternalForm());
			} else if (!textFieldURL.getText().contains("http://") && !textFieldURL.getText().contains("https://")) {
				textFieldURL.setText("http://" + textFieldURL.getText());
			}
			webView.getEngine().load(textFieldURL.getText());
		}
		textFieldURL.setText(webView.getEngine().getLocation());
	}

	@FXML
	void handleActionShoutOut(ActionEvent event) {
		webView.getEngine().executeScript("document.shoutOut()");
	}

	@FXML
	void handleActionInitialize(ActionEvent event) {
		webView.getEngine().executeScript("initialize()");
	}

	@FXML
	void handleActionNext(ActionEvent event) {
		if (webView.getEngine().getHistory().getCurrentIndex() < webView.getEngine().getHistory().getEntries().size()
				- 1) {
			webView.getEngine().getHistory().go(1);
			textFieldURL.setText(webView.getEngine().getLocation());
		}
	}

	@FXML
	void handleActionPrevious(ActionEvent event) {
		if (webView.getEngine().getHistory().getCurrentIndex() > 0) {
			webView.getEngine().getHistory().go(-1);
			textFieldURL.setText(webView.getEngine().getLocation());
		}
	}

	@FXML
	void handleActionRefresh(ActionEvent event) {
		webView.getEngine().reload();
		textFieldURL.setText(webView.getEngine().getLocation());
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		webView.getEngine().load("https://duckduckgo.com");
		textFieldURL.setText(webView.getEngine().getLocation());
	}

}
