//Mikko Mustonen 01.06.2017
package character;

import java.util.Scanner;

public class Mainclass {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Character dude = null;
		while (true) {
			System.out.println("*** TAISTELUSIMULAATTORI ***");
			System.out.println("1) Luo hahmo");
			System.out.println("2) Taistele hahmolla");
			System.out.println("0) Lopeta");
			System.out.print("Valintasi: ");
			String choice = sc.nextLine();
			try {
				int temp = Integer.parseInt(choice);
				if (temp <= 3 && temp >= 0) {
					if (temp == 0) {
						break;
					} else if (temp == 1) {
						System.out.println("Valitse hahmosi: ");
						System.out.println("1) Kuningas");
						System.out.println("2) Ritari");
						System.out.println("3) Kuningatar");
						System.out.println("4) Peikko");
						System.out.print("Valintasi: ");
						choice = sc.nextLine();
						temp = Integer.parseInt(choice);
						if (temp <= 4 && temp >= 1) {
							if (temp == 0) {
								break;
							} else if (temp == 1) {
								dude = new King();
							} else if (temp == 2) {
								dude = new Knight();
							} else if (temp == 3) {
								dude = new Queen();
							} else if (temp == 4) {
								dude = new Troll();
							}
							if (dude != null) {
								System.out.println("Valitse aseesi: ");
								System.out.println("1) Veitsi");
								System.out.println("2) Kirves");
								System.out.println("3) Miekka");
								System.out.println("4) Nuija");
								System.out.print("Valintasi: ");
								choice = sc.nextLine();
								temp = Integer.parseInt(choice);
								if (temp <= 4 && temp >= 1) {
									if (temp == 0) {
										break;
									} else if (temp == 1) {
										dude.wb = new Knife();
									} else if (temp == 2) {
										dude.wb = new Axe();
									} else if (temp == 3) {
										dude.wb = new Sword();
									} else if (temp == 4) {
										dude.wb = new Club();
									}
									continue;
								}
							}
						}
					} else if (temp == 2) {
						if (dude == null) {
							System.out.println("Luo hahmo ensin!");
						} else {
							dude.fight();
						}
						continue;
					}
				}
			} catch (NumberFormatException e) {}
			System.out.println("Väärä valinta!");
		}
		sc.close();
	}
}
