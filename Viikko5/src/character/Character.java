//Mikko Mustonen 01.06.2017
package character;

public abstract class Character {
	public WeaponBehavior wb;
	
	public Character() {
		
	}
	
	public void fight() {
		System.out.println(this.getClass().getSimpleName() + " tappelee aseella " + wb.getClass().getSimpleName());
	}
}

class King extends Character{
	
}

class Queen extends Character{
	
}

class Knight extends Character{
	
}

class Troll extends Character{
	
}

abstract class WeaponBehavior {
	
	public WeaponBehavior() {
		
	}
	
	public void useWeapon() {
		
	}
}

class Knife extends WeaponBehavior{
	
}

class Sword extends WeaponBehavior{
	
}

class Axe extends WeaponBehavior{
	
}

class Club extends WeaponBehavior{
	
}