package car;
//Mikko Mustonen 01.06.2017
import java.util.ArrayList;

public class Car {
	private String name;
	private String model;
	private Body body;
	private Chassis chassis;
	private Engine engine;
	private ArrayList<Wheel> wheels;
	private int wheelCount;
	
	public Car() {
		name = "Ford";
		model = "Focus";
		body = new Body();
		chassis = new Chassis();
		engine = new Engine();
		wheels = new ArrayList<Wheel>();
		wheelCount = 4;
		for (int i = 0; i < wheelCount; i++) {
			wheels.add(new Wheel());
		}
	}
	
	public void print() {
		System.out.println("Autoon kuuluu:");
		System.out.println("\t" + body.getClass().getName());
		System.out.println("\t" + chassis.getClass().getName());
		System.out.println("\t" + engine.getClass().getName());
		System.out.println("\t" + wheelCount + " " + wheels.get(0).getClass().getName());
	}
	
	public Car(String new_name, String new_model) {
		name = new_name;
		model = new_model;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String new_name) {
		name = new_name;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String new_model) {
		model = new_model;
	}
}

class Body {
	private String color;
	
	public Body() {
		color = "Black";
		System.out.println("Valmistetaan: " + this.getClass().getName());
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String new_color) {
		color = new_color;
	}
}

class Chassis {
	private int flex;
	
	public Chassis() {
		flex = 5;
		System.out.println("Valmistetaan: " + this.getClass().getName());
	}
	
	public int getFlex() {
		return flex;
	}
	
	public void setFlex (int new_flex) {
		flex = new_flex;
	}
}

class Engine {
	private int power;
	
	public Engine() {
		power = 250;
		System.out.println("Valmistetaan: " + this.getClass().getName());
	}
	
	public int getPower() {
		return power;
	}
	
	public void setPower(int new_power) {
		power = new_power;
	}
}

class Wheel {
	private int size;
	
	public Wheel() {
		size = 19;
		System.out.println("Valmistetaan: " + this.getClass().getName());
	}
	
	public int getSize() {
		return size;
	}
	
	public void setSize(int new_size) {
		size = new_size;
	}
}